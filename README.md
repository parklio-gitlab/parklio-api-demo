
[![N|Solid](https://parklio.com/assets/img/Parklio-Logo.svg)](https://nodesource.com/products/nsolid)

## _API Demo_

This application is a demonstration for the [Parklio API](https://parklio.com/en/parking-solutions/parklio-api) integration.

## Features
- Trigger a device remotely using the Parklio API and Parklio Gateway
- Realtime Lot Change feedback

## Prerequisite
To use this Application you need to acquire a [Parklio Barrier](https://parklio.com/en/parking-protection/parking-barrier) and [Parklio Gateway](https://parklio.com/en/parking-solutions/gateway) and receive a API test account from our team at info@parklio.com
Once you have accquired the product and a API test licence, you need to setup the devices in your account using our Parklio Connect App.

## Installation
This app was built using NodeJS v16.14.0

Install dependancies
```sh
npm install
```
Create an .env file and populate shown in the .env.example replacing the API_TOKEN and BARRIER_ID with you own. You can acquire those following the [Documentation](https://docs.parklio.com/api/v2/auth/account-user/login).
```sh
touch .env
nano .env
```
To run the app
```sh
node index.js
```
If your setup was correct this will trigger a state change in the barrier using the Parklio API and Parklio Gateway.

## Copyright
This project is licensed under the ISC license.

## Credits
Created by Parklio. Commercial support avaibale at info@parklio.com
 