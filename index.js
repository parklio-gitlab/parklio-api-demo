const fetch = require('node-fetch');
const dotenv = require('dotenv');
const { io } = require("socket.io-client");

const productOperations = {
  OPEN: 'open',
  FORCE_CLOSE: 'force_close',
  CLOSE: 'close',
  STAY_OPEN: 'stay_open',
  STAY_CLOSE: 'stay_close',
  CLEAR_STAY_OPEN: 'clear_stay_open',
  CLEAR_STAY_CLOSE: 'clear_stay_close'
};

const TIMEOUT_MINUTES = 1;

(() => {
  dotenv.config();
})();

function sendOperationRequest(operation, productId) {
  const headers = {
    'Authorization': `Bearer ${process.env.API_TOKEN}`,
    'Content-Type': 'application/json'
  };
  console.log(`Calling operation: ${operation}`);
  console.log(`ProductID: ${productId}`);
  const body = JSON.stringify({operation});
  return fetch(`${process.env.PARKLIO_URL}/products/${productId}/state`, {headers, method: 'POST', body});
};

(async () => {
  try {
    // We need to include the API_TOKEN in our Authorization header
    const headers = {'Authorization': `Bearer ${process.env.API_TOKEN}`};
    
    const res = await fetch(`${process.env.PARKLIO_URL}/barriers/${process.env.BARRIER_ID}`, {headers});

    console.log('Status Code:', res.status);
    const jsonResponse = await res.json();

    if(!jsonResponse.success) {
      console.log("Something went wrong with the request");
      jsonResponse.errors.forEach(error => {
        console.log('Error code: ', error.code);
        console.log('Error message: ', error.message);
      }); 
      return;
    }
    const {product, state} = jsonResponse.data;

    console.log('\nDevice Status');
    console.log(`Product ID: ${product.id}`);
    console.log(`Device: ${product.status}`);
    console.log(`Device State: ${state}\n`)

    // If the product is not online there is something wrong with the Parklio
    // Gateway setup. Recheck the Gateway connection and ensure it is added
    // to the Default lot of your account
    if(product.status !== 'online') {
      console.log('Device is not online, make sure that you have setup the testing enviroment correctly and that the Parklio Gateway is online and assigned to the lot.')
      return;
    }

    // We open/close the device based on its state
    let operationResponse = null;
    if(state === 'opened') {
      operationResponse = await sendOperationRequest(productOperations.CLOSE, product.id)
    } else if(state === 'closed') {
      operationResponse = await sendOperationRequest(productOperations.OPEN, product.id)
    } else {
      console.log('Barrier is not closed or open, wait for the appropriate state')
      return;
    }

    const operationResponseData = await operationResponse.json();
    // this event_id is the identifier of the ongoing operation
    const operationEventId = operationResponseData.data.event_id;

    // We setup the SocketIO client with the appropriate namespace and path
    // We need to asign the API_TOKEN in the query as authentication
    const socket = io(`${process.env.PARKLIO_URL}/realtime`, {
      path: "/socketio",
      query: {
        token: process.env.API_TOKEN
      }
    })

    socket.on("connect_error", (error) => {
      console.log('Error connecting to sockets: ', error)
    })

    socket.on("connect", () => {
      console.log("Socket Connected");
    });

    // Timeout handler that will close the app if too much times passes
    const timeoutHandler = setTimeout(() => {
      console.log('Operation Timeout');
      cleanConnection();
    }, TIMEOUT_MINUTES * 60 * 1000)

    const dotWriterInterval = setInterval(() => {
      process.stdout.write('.');
    }, 500);

    const cleanConnection = () => {
      clearInterval(dotWriterInterval);
      clearTimeout(timeoutHandler);
      socket.removeAllListeners();
      socket.close();
    }

    // Gateway Error handler
    // Will show an error and the ProductID and name of the device that 
    // triggered it
    socket.on("gateway_error", gatewayError => {
      const {data, message, type} = JSON.parse(gatewayError);

      if(data.id !== product.id) {
        return
      }

      console.log('\nOperation failed for product: ', data.name);
      console.log('Error Type: ', type);
      console.log('Error Message: ', message);
      cleanConnection();
    })

    // The lot change event is our indicator that everything went smoothly with
    // the operation. Lot changes happen whenever an advertisement of a device
    // changes. This can happen due to various events, like battery value 
    // changes, car parks on a barrier, open/close and similar. 
    // Lot changes related to the triggered state change operations will have an
    // event_id tied to the same event we recieved from the change stage request
    socket.on("lot_change", lotChangeData => {
      const {event_id} = JSON.parse(lotChangeData);

      if(!event_id) {
        return;
      }

      if(operationEventId !== event_id) {
        return;
      }

      console.log('\nOperation Finished Successfuly');
      cleanConnection();
  })

  } catch (err) {
    console.log(err.message);
  }
})();